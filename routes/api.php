<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Slidely youtube2mp3 Api Routes
|--------------------------------------------------------------------------
|
| Here is where API routes are registered. These
| routes are  assigned the "api" middleware group.
|
*/

Route::get('/youtube2mp3','YoutubeController@getToMp3');
Route::get('/youtube2mp3-2','YoutubeController@getToMp3ByYtb');
