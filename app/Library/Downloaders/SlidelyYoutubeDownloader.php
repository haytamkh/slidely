<?php
namespace App\Library\Downloaders;
use App\Library\Helpers\SlidelyCurl;

class SlidelyYoutubeDownloader{
  const YOUTUBE_BASE_URL='https://youtube.com/watch?v=';
  const YOUTUBE_PLAYER_URL='https://youtube.com/yts/jsbin/player-';
  protected $video_id;
  protected $url;
  protected $error;
  protected $video_page;
  protected $js_content;
  protected $video_info_array;



  function __construct($videoURL){
    $this->url=$videoURL;
  }

  // check if $url is a valid youtube url
  // return Boolean
  public function checkUrl()
  {
    $regex_pattern = "/(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/";
    $match;

    if(preg_match($regex_pattern, $this->url, $match))   return true;
    else{
       $this->error='Not Valid Youtube Url';
       return false;
     }

  }

  /// parse url and extract video_id
  public function parseUrl(){
    $parsed=parse_url($this->url);

    // parse query if it contain query to extract id
    if(isset($parsed['query'])) parse_str($parsed['query'],$query_params);
    else{
      $this->error="url is not youtube format";
      return false;
    }

    //extract id
    if(isset($query_params['v'])) $this->video_id=$query_params['v'];
    else{
      $this->error="url is not youtube format";
      return false;
    }

    return true;
  }

  protected function getVideoPage(){
      $video_page = file_get_contents( self::YOUTUBE_BASE_URL. $this->video_id);
      $this->video_page=$video_page;
  }

  protected function extractJSContent(){
    $playerID = explode("\/yts\/jsbin\/player-", $this->video_page);
    $playerID = $playerID[1];
    $playerURL = str_replace('\/', '/', explode('"', $playerID)[0]);
    $playerID = explode('/', $playerURL)[0];
    $js_url = self::YOUTUBE_PLAYER_URL . $playerURL;
    $js_content = file_get_contents($js_url);
    $this->js_content=$js_content;
  }

  protected function extractVideoInfo(){
    preg_match_all("/ytplayer\.config\s*=\s*(\{.+?\});/", $this->video_page, $ytplayer);
    $video_info = json_decode($ytplayer[1][0]);
    $video_array = $video_info->args;


    $loop = array('url_encoded_fmt_stream_map' => 'video', 'adaptive_fmts' => 'video_audio');
    foreach($loop as $l => $k)
    {
      foreach(explode(',', $video_array->$l) as $uefsm)
      {
        parse_str($uefsm, $video_info_array);
        parse_str($video_info_array['url'], $v_url_info);

        if(!array_key_exists('signature', $v_url_info))


        $v_info[$k][] = $video_info_array;
      }
    }
    $this->video_info_array=$video_info_array;
  }



  public function downloadGetData(){

    // parse youtube url and validate videoID
    if($this->parseUrl()){
       $tempFile=public_path().'/temp/'.$this->video_id;
    shell_exec("/usr/local/bin/youtube-dl -f 140 ".self::YOUTUBE_BASE_URL.$this->video_id." -o ".$tempFile);
    shell_exec('/usr/bin/ffmpeg -i "'.$tempFile.'" -vn -acodec libmp3lame -q:a 2 '.public_path().'/'.$this->video_id.'.mp3');
    return $this->video_id;

  }else{
    return false;
  }

  }

  public function getDownloadLink(){
    // check if is valid url
    $is_valid_url=$this->checkUrl();
    if(!$is_valid_url) return false;

    // parse url
    $is_valid_url=$this->parseUrl();
    if(!$is_valid_url) return false;

    $this->getVideoPage();
    $this->extractJSContent();
    $this->extractVideoInfo();
    $sign=$this->extractSignatureFromPlayerScript();
    // generate decrypted url
    if($sign) $this->video_info_array['url'] = urldecode($this->video_info_array['url']) . "&signature=".$sign;
    shell_exec('/usr/bin/ffmpeg -i "'.$this->video_info_array['url'].'" -vn -acodec libmp3lame -q:a 2 '.public_path().'/'.$this->video_id.'.mp3');
    return $this->video_id;
  }

  public function getError()
  {
    return $this->error;
  }


  protected function extractSignatureFromPlayerScript()
  {
    $decipherScript=$this->js_content;
    if(!isset($this->video_info_array['s'])) return(false);
    $signature=$this->video_info_array['s'];
   $signatureCall = explode('("signature",', $decipherScript);
   $callCount = count($signatureCall);
   $signatureFunction = "";
   for ($i=$callCount-1; $i > 0; $i--){
     $signatureCall[$i] = explode(');', $signatureCall[$i])[0];
     if(strpos($signatureCall[$i], '('))
     {
       $signatureFunction = explode('(', $signatureCall[$i])[0];
       break;
     }
     elseif($i==0)
     {
       die("\nFailed to get signature");
     }
   }
   $decipherPatterns = explode($signatureFunction."=function(", $decipherScript)[1];
   $decipherPatterns = explode('};', $decipherPatterns)[0];
   $deciphers = explode("(a", $decipherPatterns);
   for ($i=0; $i < count($deciphers); $i++)
   {
     $deciphers[$i] = explode('.', explode(';', $deciphers[$i])[1])[0];
     if(count(explode($deciphers[$i], $decipherPatterns))>=2)
     {
       $deciphers = $deciphers[$i];
       break;
     }
     else if($i==count($deciphers)-1)
     {
       die("\Failed to get deciphers function");
     }
   }

   $deciphersObjectVar = $deciphers;
   $decipher = explode($deciphers.'={', $decipherScript)[1];
   $decipher = str_replace(["\n", "\r"], "", $decipher);
   $decipher = explode('}};', $decipher)[0];
   $decipher = explode("},", $decipher);

   $deciphers = [];
   foreach ($decipher as &$function)
   {
     $deciphers[explode(':function', $function)[0]] = explode('){', $function)[1];
   }


   $decipherPatterns = str_replace($deciphersObjectVar.'.', '', $decipherPatterns);
   $decipherPatterns = str_replace('(a,', '->(', $decipherPatterns);
   $decipherPatterns = explode(';', explode('){', $decipherPatterns)[1]);
  $decipheredSignature =$this->executeSignaturePattern($decipherPatterns,$deciphers,$signature);

   return ($decipheredSignature);


  }



  public function executeSignaturePattern($patterns, $deciphers, $signature)
    {
      $processSignature = $signature;
      for ($i=0; $i < count($patterns); $i++) {
        //Handle non deciphers pattern
        if(strpos($patterns[$i], '->')===false){
          if(strpos($patterns[$i], '.split("")')!==false)
          {
            $processSignature = str_split($processSignature);

          }
          else if(strpos($patterns[$i], '.join("")')!==false)
          {
            $processSignature = implode('', $processSignature);

          }
          else
          {
            die("\nDecipher dictionary was not found");
          }
        }
        else
        {
          //Separate commands
          $executes = explode('->', $patterns[$i]);
          // This is parameter b value for 'function(a,b){}'
          $number = intval(str_replace(['(', ')'], '', $executes[1]));
          // Parameter a = $processSignature
          $execute = $deciphers[$executes[0]];
          //Find matched command dictionary

          switch($execute){
            case "a.reverse()":
              $processSignature = array_reverse($processSignature);
            break;
            case "var c=a[0];a[0]=a[b%a.length];a[b]=c":
              $c = $processSignature[0];
              $processSignature[0] = $processSignature[$number%count($processSignature)];
              $processSignature[$number] = $c;

            break;
            case "a.splice(0,b)":
              $processSignature = array_slice($processSignature, $number);

            break;
            default:
              die("\nDecipher dictionary was not found");
            break;
          }
        }
      }
      return $processSignature;
    }

}
