<?php
namespace App\Library\Helpers;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7;
use GuzzleHttp\Pool;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Cookie\CookieJarInterface;

class SlidelyCurl extends \GuzzleHttp\Client
{

    var $headers;
    var $response;
    var $redis;



    public function post($uri, $options = array())
    {
        $this->response=false;
       $options['headers']=is_array($options['headers']) ? $options['headers']:[];
        if ($this->headers) $options['headers'] =array_merge($this->headers,$options['headers']);


        if(isset($this->headers['http_errors']) && $this->headers['http_errors']==false) {

            $response = parent::post($uri, $options);
            return ($response);

        }else{

            try {
                $response = parent::post($uri, $options);
                if ($response) {
                    $this->response = $response;
                    return ($response->getBody());
                } else {
                    $response = array();
                    $response['error'] = array(
                        "code" => 'Down',
                        "body" => "Down",
                    );
                }
            } catch (RequestException $e) {
                $response = $this->ErrorHandle($e);
                return ($response);
            }
        }
    }






    public function get($uri, $options = array()){

        $this->response = false;
        if ($this->headers) $options['headers'] = $this->headers;
        if (isset($this->headers['http_errors']) && $this->headers['http_errors'] == false) {

            $response = parent::get($uri, $options);

            return ($response->getBody());
        } else{

            try {

                $response = parent::get($uri, $options);

                if ($response) {
                    $this->response = $response;
                    // echo "=========================================================Returned From API: " . (microtime(true) - $start);

                    return ($response->getBody());
                } else {

                    $response = array();

                    $response['error'] = array(
                        "code" => 'Down',
                        "body" => "Down",
                    );
                }
            } catch (RequestException $e) {


                $response = $this->ErrorHandle($e);
                return ($response);
            }
    }


    }


    function ErrorHandle($e)
    {
        $response = array();
        $response = $e->getResponse();
        $responseBodyAsString = $response->getBody()->getContents();

        $code = $e->getResponse()->getStatusCode();

        $body = $e->getResponse()->getBody();

        $response['error'] = array(
            "code" => $code,
            "body" => $responseBodyAsString,
        );


        return ($response);
    }

    function setErrorHandler(){
     if(is_array($this->headers)){
         $this->headers['http_errors']=false;
     }else{
         $this->headers = array(
             "http_errors" =>false,
         );
     }

    }


    function getHeader($key){
        ## get header by key
        // dd(json_$this->response);
        if(is_array($this->response)) {
            //dd($this->response['headers']);
            $header = json_decode($this->response['headers'],true);
            return($header[$key]);
        }else{
            $header=$this->response->getHeader($key);
            if(count($header)>0) return($header[0]);
            else return false;
        }
    }


    function isJSON($string){
       return is_string($string) && is_object(json_decode($string)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

}
