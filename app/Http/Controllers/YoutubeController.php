<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Library\Downloaders\SlidelyYoutubeDownloader;



class YoutubeController extends Controller
{
    //YoutubeController is responsable of all youtube routes

    public function getToMp3(Request $request){
      // $_GET['url'] is required
      if(!$request->has('url')) return response()->json(['success'=>false,'data'=>"missing url parameter"], 422);

       // start youtube downloader
      $youtube=new SlidelyYoutubeDownloader($request->get('url'));
      $videoID=$youtube->getDownloadLink();
       if(!$videoID) return response()->json(['success'=>false,'data'=>$youtube->getError()], 422);
       return response()->json(['success'=>true,'data'=>['videoID'=>$videoID,'download_url'=>$request->root().'/'.$videoID.'.mp3']]);

    }



    public function getToMp3ByYtb(Request $request){
      // $_GET['url'] is required
      if(!$request->has('url')) return response()->json(['success'=>false,'data'=>"missing url parameter"], 422);

       // start youtube downloader
      $youtube=new SlidelyYoutubeDownloader($request->get('url'));
      if(!$youtube->checkUrl()) return response()->json(['success'=>false,'data'=>"not valid youtube url"], 422);

      // download youtube video using method 1
      $videoID=$youtube->downloadGetData();
      if(!$videoID) return response()->json(['success'=>false,'data'=>$youtube->getError()], 422);
      return response()->json(['success'=>true,'data'=>['videoID'=>$videoID,'download_url'=>$request->root().'/'.$videoID.'.mp3']]);

    }
}
